#ifndef public_h
#define public_h

#define LENGTH_KERNEL    5
#define LENGTH_DOWNSAMP  2

#define LENGTH_FEATURE0    32
#define LENGTH_FEATURE1    (LENGTH_FEATURE0 - LENGTH_KERNEL + 1)
#define LENGTH_FEATURE2    (LENGTH_FEATURE1 / LENGTH_DOWNSAMP)
#define LENGTH_FEATURE3    (LENGTH_FEATURE2 - LENGTH_KERNEL + 1)
#define LENGTH_FEATURE4    (LENGTH_FEATURE3 / LENGTH_DOWNSAMP)
#define LENGTH_FEATURE5    (LENGTH_FEATURE4 - LENGTH_KERNEL + 1)

#define LAYER0            1
#define LAYER1            6
#define LAYER2            LAYER1
#define LAYER3            16
#define LAYER4            LAYER3
#define LAYER5            120
#define LAYER6          10

#define ALPHA 0.5f
#define PADDING 2


struct LeNet5
{
    float weight0_1[LAYER0][LAYER1][LENGTH_KERNEL][LENGTH_KERNEL];
    float weight2_3[LAYER2][LAYER3][LENGTH_KERNEL][LENGTH_KERNEL];
    float weight4_5[LAYER4][LAYER5][LENGTH_KERNEL][LENGTH_KERNEL];
    float weight5_6[LAYER5 * LENGTH_FEATURE5 * LENGTH_FEATURE5][LAYER6];

    float bias0_1[LAYER1];
    float bias2_3[LAYER3];
    float bias4_5[LAYER5];
    float bias5_6[LAYER6];

};

struct Feature
{
    float layer0[LAYER0][LENGTH_FEATURE0][LENGTH_FEATURE0];
    float layer1[LAYER1][LENGTH_FEATURE1][LENGTH_FEATURE1];
    float layer2[LAYER2][LENGTH_FEATURE2][LENGTH_FEATURE2];
    float layer3[LAYER3][LENGTH_FEATURE3][LENGTH_FEATURE3];
    float layer4[LAYER4][LENGTH_FEATURE4][LENGTH_FEATURE4];
    float layer5[LAYER5][LENGTH_FEATURE5][LENGTH_FEATURE5];
    float layer6[LAYER6];
};

#define THREADGROUP_SIZE 512


#define FOREACH(i,count) for (int i = 0; i < count; ++i)


#endif /* public_h */
